/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

#include "Twonk.h"
#include "Fundamental.h"

// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
	return new TwonkerAudioProcessor();
}

// ****************************************************************************
// Constructor.

TwonkerAudioProcessor::TwonkerAudioProcessor()
	: AudioProcessor(),

	// Order as defined in the header file defines the order of initialization here.
parameters(*this, nullptr, juce::Identifier("Twonker1.2"),
{
	/*
	 * * AudioParameterInt parameters are the simple case; they store the actual value. This value should
	 *		also be used for the ComboBox Ids. Zero in both cases is 'unselected'.
	 *
	 * * AudioParameterChoice parameters are zero-based and always have a default value (never NULL).
	 *		ComboBox "index" values always range from 0 upwards.
	 *		The ComboBox Ids should range from Index+1 upwards (because ComboBox Ids are one-based; zero means 'unselected').
	 *		It appears to be easier using the Index with AudioParameterChoice since they both are zero-based.
	 */
	std::make_unique<AudioParameterChoiceNotAutomatable>("Channel", "Fundamental Channel", Twonk::ChannelCBText, Twonk::ChannelCBText.indexOf("Omni", true)),
	std::make_unique<AudioParameterChoiceNotAutomatable>("Start", "Fundamental Start", Twonk::StartCBText, Twonk::StartCBText.indexOf("C4", true)),
	std::make_unique<AudioParameterChoiceNotAutomatable>("End", "Fundamental End", Twonk::EndCBText, Twonk::EndCBText.indexOf("B4", true)),
	std::make_unique<AudioParameterChoiceNotAutomatable>("ChannelWidth", "Channel Width", Twonk::ChannelWidthCBText, Twonk::ChannelWidthCBText.indexOf("12")),
	std::make_unique<AudioParameterChoice>("m2", "m2", Twonk::Minor2_CBText, 1), /* +5 */
	std::make_unique<AudioParameterChoice>("M2", "M2", Twonk::Major2_CBText, 1), /* +4 */
	std::make_unique<AudioParameterChoice>("m3", "m3", Twonk::Minor3_CBText, 1), /* -2 */
	std::make_unique<AudioParameterChoice>("M3", "M3", Twonk::Major3_CBText, 1), /* -14 */
	std::make_unique<AudioParameterChoice>("P4", "P4", Twonk::Perfect4_CBText, 1), /* -29 */
	std::make_unique<AudioParameterChoice>("D5", "D5", Twonk::Diminished5_CBText, 1), /* -49 */
	std::make_unique<AudioParameterChoice>("P5", "P5", Twonk::Perfect5_CBText, 1), /* +2 */
	std::make_unique<AudioParameterChoice>("m6", "m6", Twonk::Minor6_CBText, 1), /* +41 */
	std::make_unique<AudioParameterChoice>("M6", "M6", Twonk::Major6_CBText, 1), /* +6 */
	std::make_unique<AudioParameterChoice>("m7", "m7", Twonk::Minor7_CBText, 1), /* -31 */
	std::make_unique<AudioParameterChoice>("M7", "M7", Twonk::Major7_CBText, 1) /* -12 */
}),
mChannels(Twonk::ChannelWidthDefault)
{
	// These are "my" values (not indexes).
	int channel = parameters.getParameterAsValue("Channel").getValue().operator int();
	mFundamental.SetChannel(Twonk::ChannelValues[channel]);

	int start = parameters.getParameterAsValue("Start").getValue().operator int();
	mFundamental.SetKeyStart(Twonk::StartValues[start]);

	int end = parameters.getParameterAsValue("End").getValue().operator int();
	mFundamental.SetKeyEnd(Twonk::EndValues[end]);

	mChannelWidth = Twonk::ChannelWidthValues[parameters.getParameterAsValue("ChannelWidth").getValue().operator int()];

	mMinor2_Cents = Twonk::Minor2Values[parameters.getParameterAsValue("m2").getValue().operator int()];
	mMajor2_Cents = Twonk::Major2Values[parameters.getParameterAsValue("M2").getValue().operator int()];
	mMinor3_Cents = Twonk::Minor3Values[parameters.getParameterAsValue("m3").getValue().operator int()];
	mMajor3_Cents = Twonk::Major3Values[parameters.getParameterAsValue("M3").getValue().operator int()];
	mPerfect4_Cents = Twonk::Perfect4Values[parameters.getParameterAsValue("P4").getValue().operator int()];
	mDiminished5_Cents = Twonk::Diminished5Values[parameters.getParameterAsValue("D5").getValue().operator int()];
	mPerfect5_Cents = Twonk::Perfect5Values[parameters.getParameterAsValue("P5").getValue().operator int()];
	mMinor6_Cents = Twonk::Minor6Values[parameters.getParameterAsValue("m6").getValue().operator int()];
	mMajor6_Cents = Twonk::Major6Values[parameters.getParameterAsValue("M6").getValue().operator int()];
	mMinor7_Cents = Twonk::Minor7Values[parameters.getParameterAsValue("m7").getValue().operator int()];
	mMajor7_Cents = Twonk::Major7Values[parameters.getParameterAsValue("M7").getValue().operator int()];

	// Listeners
	parameters.addParameterListener("Channel", this);
	parameters.addParameterListener("Start", this);
	parameters.addParameterListener("End", this);
	parameters.addParameterListener("ChannelWidth", this);

	parameters.addParameterListener("m2", this);
	parameters.addParameterListener("M2", this);
	parameters.addParameterListener("m3", this);
	parameters.addParameterListener("M3", this);
	parameters.addParameterListener("P4", this);
	parameters.addParameterListener("D5", this);
	parameters.addParameterListener("P5", this);
	parameters.addParameterListener("m6", this);
	parameters.addParameterListener("M6", this);
	parameters.addParameterListener("m7", this);
	parameters.addParameterListener("M7", this);

	RequestUIUpdate();
}

// Virtual overrides that have been modified.

void TwonkerAudioProcessor::processBlock(AudioBuffer<float>& buffer, MidiBuffer& midiBuffer)
{
	int key = 0;
	int note = 0;
	int cents = 0;
	int pitchWheel = 0;
	int outChannel = 0;

	mProcessedMidi.clear();

	for (const auto meta : midiBuffer)
	{
		const MidiMessage msg = meta.getMessage();

		if (msg.isNoteOnOrOff())
		{
			key = msg.getNoteNumber();
			if (key < 9) continue; // 9 => A0

			note = key % 12;

			if (msg.isNoteOn()) {

				if (mFundamental.IsFundamental(msg.getChannel(), key)) {
					mFundamental.Push(note);

				} else {

					cents = calculatePitchbendCents(key);
					outChannel = mChannels.Allocate(key, cents);

					if (outChannel != -1) {

						// Pitch bend in cents; range is +/- two semitones, which is 200 cents in both directions.
						// 1 semitone = 100 cents.
						pitchWheel = MidiMessage::pitchbendToPitchwheelPos(static_cast<float>(cents), static_cast<float>(PITCHWHEEL_RANGE_SEMITONES * 100));

						mProcessedMidi.addEvent(MidiMessage::pitchWheel(outChannel, pitchWheel), meta.samplePosition);
						mProcessedMidi.addEvent(MidiMessage::noteOn(outChannel, key, msg.getVelocity()), meta.samplePosition);
					}
					else {
						mChannelOverload = true;
					}
				}
			}
			else { // noteOff
				if (mFundamental.IsFundamental(msg.getChannel(), key)) {
					mFundamental.Remove(note);
				}
				else {
					outChannel = mChannels.Deallocate(key);

					if (outChannel != -1) {
						mProcessedMidi.addEvent(MidiMessage::noteOff(outChannel, key, msg.getVelocity()), meta.samplePosition);
					}
				}
			}
			RequestUIUpdate();
		}
		else { // Neither noteOn nor noteOff
			mProcessedMidi.addEvent(msg, meta.samplePosition);
		}
	}
	midiBuffer.swapWith(mProcessedMidi);
}

void TwonkerAudioProcessor::parameterChanged(const String& parameterID, float newValue)
{
	// This function is called when:
	// 1) A control from the editor changes.
	// 2) An automation change is received.
	//
	int newIntValue = static_cast<int>(roundf(newValue));
	bool doUIUpdate = true;

	if (parameterID.equalsIgnoreCase("Channel")) {
		mFundamental.SetChannel(Twonk::ChannelValues[newIntValue]);
	}
	else if (parameterID.equalsIgnoreCase("Start")) {
		mFundamental.SetKeyStart(Twonk::StartValues[newIntValue]);
	}
	else if (parameterID.equalsIgnoreCase("End")) {
		mFundamental.SetKeyEnd(Twonk::EndValues[newIntValue]);
	}
	else if (parameterID.equalsIgnoreCase("ChannelWidth")) {
		mChannelWidth = Twonk::ChannelWidthValues[newIntValue];
		mChannels.ResetOutputWidth(mChannelWidth);
	}
	else if (parameterID == "m2") {
		mMinor2_Cents = Twonk::Minor2Values[newIntValue];
	}
	else if (parameterID == "M2") {
		mMajor2_Cents = Twonk::Major2Values[newIntValue];
	}
	else if (parameterID == "m3") {
		mMinor3_Cents = Twonk::Minor3Values[newIntValue];
	}
	else if (parameterID == "M3") {
		mMajor3_Cents = Twonk::Major3Values[newIntValue];
	}
	else if (parameterID == "P4") {
		mPerfect4_Cents = Twonk::Perfect4Values[newIntValue];
	}
	else if (parameterID == "D5") {
		mDiminished5_Cents = Twonk::Diminished5Values[newIntValue];
	}
	else if (parameterID == "P5") {
		mPerfect5_Cents = Twonk::Perfect5Values[newIntValue];
	}
	else if (parameterID == "m6") {
		mMinor6_Cents = Twonk::Minor6Values[newIntValue];
	}
	else if (parameterID == "M6") {
		mMajor6_Cents = Twonk::Major6Values[newIntValue];
	}
	else if (parameterID == "m7") {
		mMinor7_Cents = Twonk::Minor7Values[newIntValue];
	}
	else if (parameterID == "M7") {
		mMajor7_Cents = Twonk::Major7Values[newIntValue];
	}
	else {
		doUIUpdate = false;
	}

	if (doUIUpdate) {
		RequestUIUpdate();
	}
}

void TwonkerAudioProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
	initializePitchWheel(PITCHWHEEL_RANGE_SEMITONES);
}

// ****************************************************************************
// PUBLIC custom methods

int TwonkerAudioProcessor::GetFundamental()
{
	return mFundamental.Get();
}

bool TwonkerAudioProcessor::HasChannelOverload()
{
	return mChannelOverload;
}

CNote& TwonkerAudioProcessor::GetNoteState(int note)
{
	return mChannels.GetNoteState(note);
}

bool TwonkerAudioProcessor::IsNoteActive(int note)
{
	return mChannels.IsNoteActive(note);
}

void TwonkerAudioProcessor::ResetProcessor()
{
	mFundamental.Reset();
	mChannels.Reset();
	mProcessedMidi.clear();

	MidiDeviceInfo midiInfo = MidiOutput::getDefaultDevice();
	std::unique_ptr<MidiOutput> midiOutput = MidiOutput::openDevice(midiInfo.identifier);

	MidiBuffer buffer;
	int sample = buffer.getLastEventTime() + 1;
	
	for (int channel = 1; channel <= 16; channel++)
	{
		buffer.addEvent(MidiMessage::allNotesOff(channel), sample++);
	}

	for (const auto meta : buffer) {
		midiOutput->sendMessageNow(meta.getMessage());
	}

	mChannelOverload = false;
}

// ****************************************************************************
// Private custom methods

void TwonkerAudioProcessor::initializePitchWheel(int pitchWheelSemitoneRange)
{
	// The Twonker uses a pitch wheel range of +/- 200 cents (total of 400 cents).
	// This is the same as a range of +/- 2 semitones (a semitone being a half-step, and also 100 cents).

	// The MIDI specification says:
	// *) A Pitch Bend Range of 1 means that you can bend +/- 1 semitone.
	// *) A Pitch Bend Range of 12 means that you can bend + / -12 semitones.

	// So, configure the pitch wheel for all channels to have a range of 2 semitones.

	MidiDeviceInfo midiInfo = MidiOutput::getDefaultDevice();
	std::unique_ptr<MidiOutput> midiOutput = MidiOutput::openDevice(midiInfo.identifier);

	MidiBuffer buffer;

	for (int i = 1; i <= 16; i++) {
		buffer = MidiRPNGenerator::generate(i, 0, pitchWheelSemitoneRange);

		for (const auto meta : buffer) {
			midiOutput->sendMessageNow(meta.getMessage());
		}
	}
}

int TwonkerAudioProcessor::calculatePitchbendCents(int key)
{
	if (mFundamental.IsUnset()) return 0;

	int interval = abs((key - mFundamental.Get()) % 12);

	switch (interval) {
		case EInterval::Octave: return 0;
		case EInterval::m2: return mMinor2_Cents;
		case EInterval::M2: return mMajor2_Cents;
		case EInterval::m3: return mMinor3_Cents;
		case EInterval::M3: return mMajor3_Cents;
		case EInterval::P4: return mPerfect4_Cents;
		case EInterval::D5: return mDiminished5_Cents;
		case EInterval::P5: return mPerfect5_Cents;
		case EInterval::m6: return mMinor6_Cents;
		case EInterval::M6: return mMajor6_Cents;
		case EInterval::m7: return mMinor7_Cents;
		case EInterval::M7: return mMajor7_Cents;
		default: return 0;
	}
}

// ****************************************************************************
// Get / Set methods are from the perspective of the VST Host.

void TwonkerAudioProcessor::getStateInformation(juce::MemoryBlock& destData)
{ // Called to save state.
	auto state = parameters.copyState();
	std::unique_ptr<juce::XmlElement> xml(state.createXml());
	//std::string debugXml = xml->toString().toStdString();
	copyXmlToBinary(*xml, destData);
}

void TwonkerAudioProcessor::getCurrentProgramStateInformation(juce::MemoryBlock& destData)
{
	getStateInformation(destData); 
}

void TwonkerAudioProcessor::setStateInformation(const void* data, int sizeInBytes)
{ // Called to load state.
	std::unique_ptr<juce::XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
	//std::string debugXml = xmlState->toString().toStdString();
	if (xmlState.get() != nullptr) {
		if (xmlState->hasTagName(parameters.state.getType())) {
			parameters.replaceState(juce::ValueTree::fromXml(*xmlState));
		}
	}
}

void TwonkerAudioProcessor::setCurrentProgramStateInformation(const void* data, int sizeInBytes)
{
	setStateInformation(data, sizeInBytes);
}

// ****************************************************************************
// Virtual overrides: Defaults

// This should return at least 1, even if you're not really implementing programs.
int TwonkerAudioProcessor::getNumPrograms()
{
	return 1;
}

int TwonkerAudioProcessor::getCurrentProgram()
{
	return 0;
}

const String TwonkerAudioProcessor::getProgramName(int index)
{
	return String("");
}

void TwonkerAudioProcessor::setCurrentProgram(int index)
{
}

void TwonkerAudioProcessor::changeProgramName(int index, const String& newName)
{
}

const String TwonkerAudioProcessor::getName() const
{
	return JucePlugin_Name;
}

bool TwonkerAudioProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
	return true;
#else
	return false;
#endif
}

bool TwonkerAudioProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
	return true;
#else
	return false;
#endif
}

bool TwonkerAudioProcessor::silenceInProducesSilenceOut() const
{
	return false;
}

double TwonkerAudioProcessor::getTailLengthSeconds() const
{
	return 0.0;
}

bool TwonkerAudioProcessor::hasEditor() const
{
	return true;
}

AudioProcessorEditor* TwonkerAudioProcessor::createEditor()
{
	return new TwonkerAudioProcessorEditor(*this);
}

void TwonkerAudioProcessor::releaseResources()
{
}
