/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#pragma once

#include "Twonk.h"
#include "Note.h"
#include "Fundamental.h"

class CFundamental
{
public:
	CFundamental();

	bool IsFundamental(int channel, int key);
	bool IsUnset();
	int Get();

	void SetChannel(int channel);
	int GetChannel();
	void SetKeyStart(int key);
	int GetKeyStart();
	void SetKeyEnd(int key);
	int GetKeyEnd();

	void Push(int note);
	void Remove(int note);

	void Reset();

private:
	int mKeyStart = 0;
	int mKeyEnd = 0;
	int mChannel = 0;
	int mFundamental = NOTE_UNSET;

	static const int STACK_SIZE = 16; // 12-tones plus 4 as a buffer
	int mStack[STACK_SIZE];
	int mCount = 0;

	void MergeOver(int i);
};
