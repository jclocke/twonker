/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#include "Twonk.h"
#include "Fundamental.h"

CFundamental::CFundamental()
{
	Reset();
}

void CFundamental::SetChannel(int channel) { mChannel = channel; }
int CFundamental::GetChannel() { return mChannel; }

void CFundamental::SetKeyStart(int key) { mKeyStart = key; }
int CFundamental::GetKeyStart() { return mKeyStart; }

void CFundamental::SetKeyEnd(int key) {	mKeyEnd = key; }
int CFundamental::GetKeyEnd() { return mKeyEnd; }

void CFundamental::Reset()
{
	for (int i = 0; i < STACK_SIZE; i++) mStack[i] = NOTE_UNSET;
	mFundamental = NOTE_UNSET;
	mCount = 0;
}

bool CFundamental::IsFundamental(int channel, int key)
{
	if (key < 9) return false; // 9 => A0

	if ((key >= mKeyStart) && (key <= mKeyEnd) &&
		((mChannel == FUNDAMENTAL_CHANNEL_OMNI) || (channel == mChannel))
		) {
		return true;
	}
	else {
		return false;
	}
}

bool CFundamental::IsUnset()
{
	return mFundamental == NOTE_UNSET;
}

int CFundamental::Get()
{
	return mFundamental;
}

void CFundamental::Push(int note)
{
	Remove(note);

	// Add it to the front.
	for (int i = mCount; i > 0; i--) {
		mStack[i] = mStack[i - 1];
	}
	mStack[0] = note;

	mCount++;
	mFundamental = note;
}

void CFundamental::Remove(int note)
{
	// Remove the note from the list.
	// Merge down to pack everything contiguously.

	for (int i = 0; i < mCount; i++) {

		if (mStack[i] == note) {
			MergeOver(i);
		}
	}
	mFundamental = mStack[0];
}

void CFundamental::MergeOver(int index)
{
	for (int i = index; i < mCount; i++) {
		mStack[i] = mStack[i + 1];
	}
	mCount--;
}
