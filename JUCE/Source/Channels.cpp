/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#include "Channels.h"

void CChannel::Reset()
{
	Note = NOTE_UNSET;
	Cents = 0;
	Active = 0;
}

CChannels::CChannels(int width)
{
	mChannelWidth = (width > 12 ? 12 : width);
	Reset();
}

void CChannels::Reset()
{
	for (int _channel = 1; _channel <= mChannelWidth; _channel++) // Zero not used; ranges from 1 -> 12.
	{
		mChannelNotes[_channel].Reset();
	}

	for (int note = 0; note < 12; note++) {
		mActiveNotes[note].Reset();
	}
}

void CChannels::ResetOutputWidth(int width)
{
	mChannelWidth = width;
}

int CChannels::NextFree()
{
	for (int _channel = 1; _channel <= mChannelWidth; _channel++)
	{
		if (mChannelNotes[_channel].Active <= 0) { // Found a channel.
			return _channel;
		}
	}
	return -1; // Overload!
}

int CChannels::Allocate(int key, int cents)
{
	int _note = key % 12;

	// If the note in another octave is already being played, return that channel and reuse.
	if (mActiveNotes[_note].Active > 0)
	{
		int _channel = mActiveNotes[_note].Channel;

		mChannelNotes[_channel].Active++;
		mActiveNotes[_note].Active++;

		return _channel;
	}
	else {
		int nextFree = NextFree();

		if (nextFree == -1) {
			// Overload !
			return -1;
		}

		// Allocated new channel
		mChannelNotes[nextFree].Note = _note;
		mChannelNotes[nextFree].Cents = cents;
		mChannelNotes[nextFree].Active++;

		mActiveNotes[_note].Channel = nextFree;
		mActiveNotes[_note].Cents = cents;
		mActiveNotes[_note].Active++;

		return nextFree;
	}
}

int CChannels::Deallocate(int key)
{
	int _note = key % 12;

	// If the note is being played, return that channel.
	if (mActiveNotes[_note].Active > 0)
	{
		int _channel = mActiveNotes[_note].Channel;

		if (--mChannelNotes[_channel].Active <= 0) {
			mChannelNotes[_channel].Note = NOTE_UNSET;
			mChannelNotes[_channel].Cents = 0;
		}

		if (--mActiveNotes[_note].Active <= 0) {
			mActiveNotes[_note].Channel = 0;
			mActiveNotes[_note].Cents = 0;
		}

		return _channel;
	}
	else {
		return -1; // Not found.
	}
}

bool CChannels::IsNoteActive(int note)
{
	return mActiveNotes[note].Active > 0;
}

CNote& CChannels::GetNoteState(int note)
{
	return mActiveNotes[note];
}
