/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#include "Note.h"

#include <string>
#include <math.h>

int CNote::GetKey(std::string noteString)
{
	int note;
	int octave;

	note = CNote::GetNote(noteString);
	octave = (int)(noteString[noteString.size() - 1] - '0');

	return ((octave * 12) + note);
}

void CNote::Reset()
{
	Active = 0;
	Channel = 0;
	Cents = 0;
	mNote = 0;
	mOctave = 0;
	mKey = 0;
}

int CNote::GetNote(std::string noteString)
{
	static int note;
	switch (noteString[0])
	{
		case 'C': note = ENote::C; break;
		case 'D': note = ENote::D; break;
		case 'E': note = ENote::E; break;
		case 'F': note = ENote::F; break;
		case 'G': note = ENote::G; break;
		case 'A': note = ENote::A; break;
		case 'B': note = ENote::B; break;
	}
	if (noteString[1] == '#') note = (note + 1) % 12;
	return note;
}

void CNote::GetNote(CNote& note, std::string noteString)
{
	static int noteEnum = CNote::GetNote(noteString);
	note.SetNote(noteEnum, (int)(noteString[noteString.size() - 1] - '0'));
}

std::string CNote::GetNoteString(int key)
{
	std::string s;
	switch (key % 12)
	{
		case C: s = "C"; break;
		case CS: s = "C#"; break;
		case D: s = "D"; break;
		case DS: s = "D#"; break;
		case E: s = "E"; break;
		case F: s = "F"; break;
		case FS: s = "F#"; break;
		case G: s = "G"; break;
		case GS: s = "G#"; break;
		case A: s = "A"; break;
		case AS: s = "A#"; break;
		case B: s = "B"; break;
		default: s = "";  break;
	}
	return s;
}

void CNote::SetNote(int note, int octave)
{
	mNote = note;
	mOctave = (octave > DEFAULT_OCTAVE_COUNT ? DEFAULT_OCTAVE_COUNT : octave);
	mKey = (mOctave * 12) + note;
}

void CNote::SetNote(int key)
{
	mNote = (ENote)(key % 12);
	mOctave = CNote::GetOctave(key);
	mKey = key;
}

void CNote::SetNote(std::string note)
{
	mNote = CNote::GetNote(note);
	mOctave = (int)(note[note.size()] -'0');
	mKey = mNote * mOctave;
}

