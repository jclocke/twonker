/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#pragma once

#include <string>
#include <cmath>

#define DEFAULT_OCTAVE_COUNT 11
#define DEFAULT_SCALE_COUNT (DEFAULT_OCTAVE_COUNT * 12)

enum ENote { NOTE_UNSET = -1, C = 0, CS = 1, D = 2, DS = 3, E = 4, F = 5, FS = 6, G = 7, GS = 8, A = 9, AS = 10, B = 11 };
enum EInterval { INTERVAL_UNSET = -1, Octave = 0, m2 = 1, M2 = 2, m3 = 3, M3 = 4, P4 = 5, D5 = 6, P5 = 7, m6 = 8, M6 = 9, m7 = 10, M7 = 11 };

class CNote
{
public:
	int Active = 0;
	int Channel = -1;
	int Cents = 0;

	static int GetKey(std::string note);
	static std::string GetNoteString(int note);

	static void GetNote(CNote& note, std::string noteString);
	static int GetNote(std::string noteString);

	void SetNote(int note, int octave);
	void SetNote(int key);
	void SetNote(std::string note);

	void Reset();

private:
	int mNote = 0;
	int mOctave = 0;
	int mKey = 0;

	static inline int GetOctave(int key) { return static_cast<int>(floor((float)key / 12)); }
};
