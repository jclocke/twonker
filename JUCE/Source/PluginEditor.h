/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 6.1.6

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2020 - Raw Material Software Limited.

  ==============================================================================
*/

#pragma once

//[Headers]     -- You can add your own extra header files here --
/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/
#include "JuceHeader.h"
#include "PluginProcessor.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
                                                                    //[/Comments]
*/
class TwonkerAudioProcessorEditor  : public AudioProcessorEditor,
                                     public Timer,
                                     public juce::Button::Listener,
                                     public juce::ComboBox::Listener
{
public:
    //==============================================================================
    TwonkerAudioProcessorEditor (TwonkerAudioProcessor& ownerFilter);
    ~TwonkerAudioProcessorEditor() override;

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
		void timerCallback() override;
		void InitializeControls();
    //[/UserMethods]

    void paint (juce::Graphics& g) override;
    void resized() override;
    void buttonClicked (juce::Button* buttonThatWasClicked) override;
    void comboBoxChanged (juce::ComboBox* comboBoxThatHasChanged) override;

    // Binary resources:
    static const char* twonkeroff_png;
    static const int twonkeroff_pngSize;
    static const char* twonkeron_png;
    static const int twonkeron_pngSize;
    static const char* circuitboard_jpg;
    static const int circuitboard_jpgSize;
    static const char* logo_png;
    static const int logo_pngSize;
    static const char* noteOn_png;
    static const int noteOn_pngSize;
    static const char* noteOff_png;
    static const int noteOff_pngSize;
    static const char* twonkNoteOn_png;
    static const int twonkNoteOn_pngSize;
    static const char* overload_redresized_png;
    static const int overload_redresized_pngSize;
    static const char* overload_greenresized_png;
    static const int overload_greenresized_pngSize;


private:
    //[UserVariables]   -- You can add your own custom variables in this section.
		TwonkerAudioProcessor& mTwonkerProcessor;

		juce::ImageButton* onLights[12];
		juce::ImageButton* twonkLights[12];
		juce::Label* offsets[12];
		juce::Label* intervals[12];

    void ResetEditor();
    //[/UserVariables]

    //==============================================================================
    std::unique_ptr<juce::GroupComponent> imgFlatOff;
    std::unique_ptr<juce::ImageButton> imgLogo;
    std::unique_ptr<juce::GroupComponent> gbFundamental2;
    std::unique_ptr<juce::ImageButton> imgChannelOverloadOff;
    std::unique_ptr<juce::ImageButton> imgChannelOverloadOn;
    std::unique_ptr<juce::GroupComponent> gbIndicators;
    std::unique_ptr<juce::GroupComponent> gbAlterations;
    std::unique_ptr<juce::GroupComponent> gbFundamental;
    std::unique_ptr<juce::ImageButton> C_off;
    std::unique_ptr<juce::ImageButton> C_twonk;
    std::unique_ptr<juce::ImageButton> C_on;
    std::unique_ptr<juce::Label> lblC;
    std::unique_ptr<juce::ImageButton> B_off;
    std::unique_ptr<juce::ImageButton> B_twonk;
    std::unique_ptr<juce::ImageButton> B_on;
    std::unique_ptr<juce::ImageButton> As_off;
    std::unique_ptr<juce::ImageButton> As_twonk;
    std::unique_ptr<juce::ImageButton> As_on;
    std::unique_ptr<juce::ImageButton> A_off;
    std::unique_ptr<juce::ImageButton> A_twonk;
    std::unique_ptr<juce::ImageButton> A_on;
    std::unique_ptr<juce::ImageButton> Gs_off;
    std::unique_ptr<juce::ImageButton> Gs_twonk;
    std::unique_ptr<juce::ImageButton> Gs_on;
    std::unique_ptr<juce::ImageButton> G_off;
    std::unique_ptr<juce::ImageButton> G_twonk;
    std::unique_ptr<juce::ImageButton> G_on;
    std::unique_ptr<juce::ImageButton> Fs_off;
    std::unique_ptr<juce::ImageButton> Fs_twonk;
    std::unique_ptr<juce::ImageButton> Fs_on;
    std::unique_ptr<juce::ImageButton> F_off;
    std::unique_ptr<juce::ImageButton> F_twonk;
    std::unique_ptr<juce::ImageButton> F_on;
    std::unique_ptr<juce::ImageButton> E_off;
    std::unique_ptr<juce::ImageButton> E_twonk;
    std::unique_ptr<juce::ImageButton> E_on;
    std::unique_ptr<juce::ImageButton> Ds_off;
    std::unique_ptr<juce::ImageButton> Ds_twonk;
    std::unique_ptr<juce::ImageButton> Ds_on;
    std::unique_ptr<juce::ImageButton> D_off;
    std::unique_ptr<juce::ImageButton> D_twonk;
    std::unique_ptr<juce::ImageButton> D_on;
    std::unique_ptr<juce::ImageButton> Cs_off;
    std::unique_ptr<juce::ImageButton> Cs_twonk;
    std::unique_ptr<juce::ImageButton> Cs_on;
    std::unique_ptr<juce::ImageButton> imgTwonkOff;
    std::unique_ptr<juce::ImageButton> imgTwonkOn;
    std::unique_ptr<juce::Label> lblM7;
    std::unique_ptr<juce::Label> lblStart;
    std::unique_ptr<juce::Label> lblm2;
    std::unique_ptr<juce::Label> lblm3;
    std::unique_ptr<juce::Label> lblP4;
    std::unique_ptr<juce::Label> lblm6;
    std::unique_ptr<juce::Label> lblm7;
    std::unique_ptr<juce::Label> lblM2;
    std::unique_ptr<juce::Label> lblM3;
    std::unique_ptr<juce::Label> lblD5;
    std::unique_ptr<juce::Label> lblM6;
    std::unique_ptr<juce::Label> lblP5;
    std::unique_ptr<juce::Label> lblChannel;
    std::unique_ptr<juce::Label> lblEnd;
    std::unique_ptr<juce::Label> lblFundamental;
    std::unique_ptr<juce::Label> lblB;
    std::unique_ptr<juce::Label> lblAs;
    std::unique_ptr<juce::Label> lblA;
    std::unique_ptr<juce::Label> lblGs;
    std::unique_ptr<juce::Label> lblG;
    std::unique_ptr<juce::Label> lblFs;
    std::unique_ptr<juce::Label> lblF;
    std::unique_ptr<juce::Label> lblE;
    std::unique_ptr<juce::Label> lblDs;
    std::unique_ptr<juce::Label> lblD;
    std::unique_ptr<juce::Label> lblCs;
    std::unique_ptr<juce::Label> B_offset;
    std::unique_ptr<juce::Label> A_offset;
    std::unique_ptr<juce::Label> As_offset;
    std::unique_ptr<juce::Label> Gs_offset;
    std::unique_ptr<juce::Label> F_offset;
    std::unique_ptr<juce::Label> Fs_offset;
    std::unique_ptr<juce::Label> G_offset;
    std::unique_ptr<juce::Label> C_offset;
    std::unique_ptr<juce::Label> D_offset;
    std::unique_ptr<juce::Label> Cs_offset;
    std::unique_ptr<juce::Label> Ds_offset;
    std::unique_ptr<juce::Label> E_offset;
    std::unique_ptr<juce::ComboBox> cbChannel;
    std::unique_ptr<juce::ComboBox> cbStart;
    std::unique_ptr<juce::ComboBox> cbEnd;
    std::unique_ptr<juce::ComboBox> cbm2;
    std::unique_ptr<juce::ComboBox> cbM2;
    std::unique_ptr<juce::ComboBox> cbm3;
    std::unique_ptr<juce::ComboBox> cbM3;
    std::unique_ptr<juce::ComboBox> cbP4;
    std::unique_ptr<juce::ComboBox> cbD5;
    std::unique_ptr<juce::ComboBox> cbP5;
    std::unique_ptr<juce::ComboBox> cbm6;
    std::unique_ptr<juce::ComboBox> cbM6;
    std::unique_ptr<juce::ComboBox> cbm7;
    std::unique_ptr<juce::ComboBox> cbM7;
    std::unique_ptr<juce::Label> lblChannelWidth;
    std::unique_ptr<juce::ComboBox> cbChannelWidth;
    std::unique_ptr<juce::Label> lblVersion;
    std::unique_ptr<juce::Label> lblMeDotCom;
    juce::Image cachedImage_circuitboard_jpg_1;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TwonkerAudioProcessorEditor)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

