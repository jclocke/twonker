/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#include "../../JUCE/Source/Twonk.h"
#include "../../JUCE/Source/Fundamental.h"
#include "../../JUCE/Source/Note.h"
#include "../../JUCE/Source/Channels.h"

// ******************************************************************

class AudioParameterIntNotAutomatable : public AudioParameterInt
{
public:
	AudioParameterIntNotAutomatable(const String& parameterID, const String& parameterName, int minValue, int maxValue, int defaultValue
	) : AudioParameterInt(parameterID, parameterName, minValue, maxValue, defaultValue) {}
	bool isAutomatable() const override { return false; }
};

class AudioParameterChoiceNotAutomatable : public AudioParameterChoice
{
public:
	AudioParameterChoiceNotAutomatable(const String& parameterID, const String& parameterName, const StringArray& choices,	int defaultItemIndex
	) : AudioParameterChoice(parameterID, parameterName, choices, defaultItemIndex) {}
	bool isAutomatable() const override { return false; }
};

// ******************************************************************

class TwonkerAudioProcessor : public AudioProcessor,
                              public juce::AudioProcessorValueTreeState::Listener
{
public:
	TwonkerAudioProcessor();

	AudioProcessorValueTreeState parameters;

	bool NeedsUIUpdate(){ return mUIUpdateFlag; };
	void RequestUIUpdate(){ mUIUpdateFlag = true; };
	void ClearUIUpdateFlag(){ mUIUpdateFlag = false; };

	int GetFundamental();
	bool IsNoteActive(int note);
	CNote& GetNoteState(int note);

	bool HasChannelOverload();

	// ***************************************************************************

	void parameterChanged(const String& idIn, float valueIn) override;
	void processBlock(AudioBuffer<float>&, MidiBuffer&) override;

	AudioProcessorEditor* createEditor() override;
	bool hasEditor() const override;

	const String getName() const override;

	bool acceptsMidi() const override;
	bool producesMidi() const override;
	bool silenceInProducesSilenceOut() const override;
	double getTailLengthSeconds() const override;

	// State and Program Data
	void getStateInformation(juce::MemoryBlock& destData) override;
	void getCurrentProgramStateInformation(juce::MemoryBlock& destData) override;

	void setStateInformation(const void* data, int sizeInBytes) override;
	void setCurrentProgramStateInformation(const void* data, int sizeInBytes) override;

	int getNumPrograms() override;
	int getCurrentProgram() override;
	const String getProgramName(int index) override;
	void setCurrentProgram(int index) override;
	void changeProgramName(int index, const String& newName) override;
	void prepareToPlay(double sampleRate, int samplesPerBlock) override;
	void releaseResources() override;

	void ResetProcessor();

	// ***************************************************************************

private:
	CFundamental mFundamental;
	int mChannelWidth;

	int mMinor2_Cents;
	int mMajor2_Cents;
	int mMinor3_Cents;
	int mMajor3_Cents;
	int mPerfect4_Cents;
	int mDiminished5_Cents;
	int mPerfect5_Cents;
	int mMinor6_Cents;
	int mMajor6_Cents;
	int mMinor7_Cents;
	int mMajor7_Cents;

	CChannels mChannels{ 12 };
	MidiBuffer mProcessedMidi;

	void initializePitchWheel(int pitchWheelSemitoneRange);
	int calculatePitchbendCents(int key);

	bool mUIUpdateFlag;
	bool mChannelOverload = false;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TwonkerAudioProcessor)
};
