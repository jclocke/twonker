/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#include "JuceHeader.h"

#include "PluginProcessor.h"
#include "Twonk.h"

const juce::StringArray Twonk::ChannelCBText = { "Omni", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16" };
const int Twonk::ChannelValues[17] = { FUNDAMENTAL_CHANNEL_OMNI, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
const int Twonk::ChannelDefault = Twonk::ChannelCBText.indexOf("Omni", true);

const juce::StringArray Twonk::StartCBText = { "C1", "C2", "C3", "C4", "C5", "C6", "C7" };
const int Twonk::StartValues[7] =
{
	CNote::GetKey("C1"),
	CNote::GetKey("C2"),
	CNote::GetKey("C3"),
	CNote::GetKey("C4"),
	CNote::GetKey("C5"),
	CNote::GetKey("C6"),
	CNote::GetKey("C7")
};
const int Twonk::StartDefault = CNote::GetKey("C4");

const juce::StringArray Twonk::EndCBText = { "B1", "B2", "B3", "B4", "B5", "B6", "B7" };
const int Twonk::EndValues[7] =
{
	CNote::GetKey("B1"),
	CNote::GetKey("B2"),
	CNote::GetKey("B3"),
	CNote::GetKey("B4"),
	CNote::GetKey("B5"),
	CNote::GetKey("B6"),
	CNote::GetKey("B7")
};
const int Twonk::EndDefault = CNote::GetKey("B4");

const juce::StringArray Twonk::ChannelWidthCBText = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
const int Twonk::ChannelWidthValues[12] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
const int Twonk::ChannelWidthDefault = Twonk::ChannelWidthCBText.indexOf("12");

const juce::StringArray Twonk::Minor2_CBText = { "-", "+5" };
const int Twonk::Minor2Values[2] = { 0, 5 };
const int Twonk::Minor2Default = 1;

const juce::StringArray Twonk::Major2_CBText = { "-", "+4" };
const int Twonk::Major2Values[2] = { 0, 4 };
const int Twonk::Major2Default = 1;

const juce::StringArray Twonk::Minor3_CBText = { "-", "-2" };
const int Twonk::Minor3Values[2] = { 0, -2 };
const int Twonk::Minor3Default = 1;

const juce::StringArray Twonk::Major3_CBText = { "-", "-14" };
const int Twonk::Major3Values[2] = { 0, -14 };
const int Twonk::Major3Default = 1;

const juce::StringArray Twonk::Perfect4_CBText = { "-", "-29" };
const int Twonk::Perfect4Values[2] = { 0, -29 };
const int Twonk::Perfect4Default = 1;

const juce::StringArray Twonk::Diminished5_CBText = { "-", "-49", "+28" };
const int Twonk::Diminished5Values[3] = { 0, -49, 28 };
const int Twonk::Diminished5Default = 1;

const juce::StringArray Twonk::Perfect5_CBText = { "-", "+2" };
const int Twonk::Perfect5Values[2] = { 0, 2 };
const int Twonk::Perfect5Default = 1;

const juce::StringArray Twonk::Minor6_CBText = { "-", "+41", "-27" };
const int Twonk::Minor6Values[3] = { 0, 41, -27 };
const int Twonk::Minor6Default = 1;

const juce::StringArray Twonk::Major6_CBText = { "-", "+6" };
const int Twonk::Major6Values[2] = { 0, 6 };
const int Twonk::Major6Default = 1;

const juce::StringArray Twonk::Minor7_CBText = { "-", "-31", "+31" };
const int Twonk::Minor7Values[3] = { 0, -31, 31 };
const int Twonk::Minor7Default = 1;

const juce::StringArray Twonk::Major7_CBText = { "-", "-12", "+45" };
const int Twonk::Major7Values[3] = { 0, -12, 45 };
const int Twonk::Major7Default = 1;
