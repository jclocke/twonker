/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#pragma once

#include "Note.h"

class CChannel
{
public:
	int Note = NOTE_UNSET;
	int Cents = 0;
	int Active = 0;
	void Reset();
};

class CChannels
{
public:
	CChannels(int width);

	void ResetOutputWidth(int width);
	int NextFree();

	int Allocate(int key, int cents);
	int Deallocate(int key);

	bool IsNoteActive(int note);
	CNote& GetNoteState(int note);

	void Reset();

private:
	CChannel mChannelNotes[13]; // One-based; ranges from 1 -> 12 (zero not used).
	CNote mActiveNotes[12];

	int mChannelWidth = 0;
};
