/*
	Twonker: MIDI software for playing overtones.
	Copyright(C) 2015,2023 Jason Locke <miditwonker@outlook.com>
	https://jasonlocke.com/twonker

	This module is free software.  You can redistribute it and/or
	modify it under the terms of the Artistic License 2.0.

	This program is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.
*/

#pragma once

#include "JuceHeader.h"

#define PITCHWHEEL_RANGE_SEMITONES 2
#define FUNDAMENTAL_CHANNEL_OMNI 17

class Twonk
{
public:
	static const juce::StringArray ChannelCBText;
	static const int ChannelValues[];
	static const int ChannelDefault;

	static const juce::StringArray StartCBText;
	static const int StartValues[];
	static const int StartDefault;

	static const juce::StringArray EndCBText;
	static const int EndValues[];
	static const int EndDefault;

	static const juce::StringArray ChannelWidthCBText;
	static const int ChannelWidthValues[];										
	static const int ChannelWidthDefault;

	static const juce::StringArray Minor2_CBText;
	static const int Minor2Values[];																
	static const int Minor2Default;

	static const juce::StringArray Major2_CBText;
	static const int Major2Values[];																
	static const int Major2Default;

	static const juce::StringArray Minor3_CBText;
	static const int Minor3Values[];																
	static const int Minor3Default;

	static const juce::StringArray Major3_CBText;
	static const int Major3Values[];																
	static const int Major3Default;

	static const juce::StringArray Perfect4_CBText;
	static const int Perfect4Values[];																
	static const int Perfect4Default;

	static const juce::StringArray Diminished5_CBText;
	static const int Diminished5Values[];																
	static const int Diminished5Default;

	static const juce::StringArray Perfect5_CBText;
	static const int Perfect5Values[];																
	static const int Perfect5Default;

	static const juce::StringArray Minor6_CBText;
	static const int Minor6Values[];																
	static const int Minor6Default;

	static const juce::StringArray Major6_CBText;
	static const int Major6Values[];																
	static const int Major6Default;

	static const juce::StringArray Minor7_CBText;
	static const int Minor7Values[];																
	static const int Minor7Default;

	static const juce::StringArray Major7_CBText;
	static const int Major7Values[];
	static const int Major7Default;
};
