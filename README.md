﻿# The Twonker

[https://jasonlocke.com/twonker](https://jasonlocke.com/twonker)

The Twonker is a free VST MIDI Plugin that allows a digital piano (or any MIDI instrument) to play tones from the **Overtone Series** (also known as the [Harmonic Series](http://en.wikipedia.org/wiki/Harmonic_series_(music))). It is designed to run within most Digital Audio Workstations as a Synth Plugin on MacOS, Windows, and Linux.

The Twonker allows a musician to play overtones in a predictable and flexible manner, using a regular MIDI-based keyboard. In essence, it behaves like twelve different pianos tuned to twelve different "Overtone Scales".

## What are Overtones?

When a length of string (or a column of air) is made to vibrate it produces a pitch. This initial pitch is called the Fundamental.

While the string is vibrating at the Fundamental frequency, the string simultaneously vibrates at many other frequencies as well. The pattern of frequencies is always the same and is based on the frequency of the Fundamental itself.

In addition to the Fundamental, the string vibrates at a second frequency which is one-half the wavelength of the Fundamental (or double in pitch; which is an octave). This second frequency is called the First Overtone. The pattern repeats as the string also vibrates at another frequency which is one-third the wavelength of the First Overtone. This third frequency is called the Second Overtone.

The process continues indefinitely, diminishing in volume as the pattern repeats. The pattern of notes is referred to as the Overtone Series.

## What is Equal Temperament?

Equal Temperament is a system of musical tuning in which each octave is divided into twelve equal-sized intervals. A typical piano is an example of an Equal Tempered instrument. Each octave is divided into twelve half-steps; each being the same size.

In Equal Temperament, changing keys is easy and does not require that the instrument be retuned. For example, the C Chromatic Scale uses the same twelve tones as the G Chromatic Scale. It's easy to change keys since each key contains notes from the same twelve frequencies.

## What is Just Temperament?

In Just Temperament, scales are built with tones from the Overtone Series. In comparison with Equal Temperament however, Just Temperament does not have equal-sized intervals (except for the octave).

Given that the intervals between tones in Just Temperament are not equal-sized, scales based on Just Temperament contain totally different tones when compared with each other.

In order to allow multiple keys in Just Temperament, you would need to build twelve pianos; each tuned to a different Fundamental. To change key from C to G, run over to the G piano and wail away.

## Enter: The Twonker

The Twonker allows a musician to play overtones in a predictable manner, using a regular MIDI-based keyboard. In essence, it behaves like twelve different pianos tuned to twelve different Just Tempered Scales. You change keys in Just Temperament by simply choosing a new Fundamental and otherwise playing normally.

[https://jasonlocke.com/twonker](https://jasonlocke.com/twonker)
