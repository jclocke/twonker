#!/bin/bash
#
# The Windows and MacOS binaries must be placed in the ./bin folder prior
# to running this script (the Linux binary will be grabbed from the
# Makefile generated location).
#
#########################################################################

if [ ! -d build ]; then
  echo "Created ./build directory"
	mkdir build
fi

rm -rf ./build/*

# Check that all binaries exist; bail otherwise
if [ ! -f ./bin/Windows/Twonker.vst3 ]; then
	echo "Windows binary not found (./bin/Windows/Twonker.vst3).. exiting"
	exit 1
fi

if [ ! -d ./bin/MacOS/Twonker.vst3 ]; then
	echo "MacOS binary not found (./bin/MacOS/Twonker.vst3).. exiting"
fi

if [ ! -f ./bin/Windows/Twonker.vst3 ]; then
	echo "Linux binary not found (./bin/Windows/Twonker.vst3).. exiting"
	exit 1
fi

VERSION=`cat VERSION`
echo "Building source tarball"

mkdir ./build/Twonker_Src_${VERSION}
find . -maxdepth 1 -not -name '.' -not -name '.git' -not -name build -not -name bin | xargs cp -pr --target-directory ./build/Twonker_Src_${VERSION}

tar -C ./build -X source_exclude -czf ./build/Twonker_Src_${VERSION}.tgz Twonker_Src_${VERSION} > /dev/null 2>&1

if [ $? -ne 0 ]; then
	echo Source tarball failed.. exiting
	exit 1
fi

echo "Signing source tarball"
gpg --detach-sign --default-key=A974F9A2 ./build/Twonker_Src_${VERSION}.tgz > /dev/null 2>&1

if [ $? -ne 0 ]; then
	echo Signing the source tarball failed.. exiting
	exit 1
fi

echo "Clearing temporary source directory"
rm -rf ./build/Twonker_Src_${VERSION}

echo "Zipping Windows binary"
zip -j ./build/Twonker_${VERSION}_win.zip ./bin/Windows/Twonker.vst3 > /dev/null 2>&1

if [ $? -ne 0 ]; then
	echo Generating the Windows zipfile failed.. exiting
	exit 1
fi

cd ./bin/MacOS
echo "Zipping MacOS binary"
zip -r ../../build/Twonker_${VERSION}_macos.zip Twonker.vst3

if [ $? -ne 0 ]; then
	echo Generating the MacOS zipfile failed.. exiting
  cd ../../
	exit 1
fi
cd ../../

cd ./bin/Linux
echo "Zipping Linux binary"
zip -r ../../build/Twonker_${VERSION}_linux.zip Twonker.vst3

if [ $? -ne 0 ]; then
	echo Generating the Linux zipfile failed.. exiting
	exit 1
fi
cd ../../

echo "Signing Windows binary"
gpg --detach-sign --default-key=A974F9A2 ./build/Twonker_${VERSION}_win.zip > /dev/null 2>&1

if [ $? -ne 0 ]; then
	echo Signing the Windows zipfile failed.. exiting
	exit 1
fi

echo "Signing MacOS binary"
gpg --detach-sign --default-key=A974F9A2 ./build/Twonker_${VERSION}_macos.zip > /dev/null 2>&1

if [ $? -ne 0 ]; then
	echo Signing the MacOS zipfile failed.. exiting
	exit 1
fi

echo "Signing Linux binary"
gpg --detach-sign --default-key=A974F9A2 ./build/Twonker_${VERSION}_linux.zip > /dev/null 2>&1

if [ $? -ne 0 ]; then
	echo Signing the Linux tarball failed.. exiting
	exit 1
fi

echo Success
